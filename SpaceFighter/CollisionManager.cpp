
#include "CollisionManager.h"


void CollisionManager::AddCollisionType(const CollisionType type1, const CollisionType type2, OnCollision callback)
{
	Collision c;
	c.Type1 = (type1 < type2) ? type1 : type2;
	c.Type2 = (type1 > type2) ? type1 : type2;
	c.Callback = callback;

	m_collisions.push_back(c);
}

/// <summary>
/// Checks if two GameObjects are colliding.
/// </summary>
/// <param name="pGameObject1"></param>
/// <param name="pGameObject2"></param>
void CollisionManager::CheckCollision(GameObject *pGameObject1, GameObject *pGameObject2)
{
	CollisionType t1 = pGameObject1->GetCollisionType();
	CollisionType t2 = pGameObject2->GetCollisionType();

	// Ignore collisions between two of the same CollisionTypes or if either
	// GameObject has CollisionType::NONE
	if (t1 == t2 || t1 == CollisionType::NONE || t2 == CollisionType::NONE) return;

	bool swapped = false;
	// Set lowest value CollectionType to t1 to make comparisons easier later
	if (t1 > t2)
	{
		std::swap(t1, t2);
		swapped = true;
	}

	m_nonCollisionIt = m_nonCollisions.begin();
	// Check if we're ignoring collisions between the t1 and t2 types.
	for (; m_nonCollisionIt != m_nonCollisions.end(); m_nonCollisionIt++)
	{
		NonCollision nc = *m_nonCollisionIt;
		// If t1 and t2 CollisionTypes are NonCollisions, stop processing this collision.
		if ((nc.Type1 == t1 && nc.Type2 == t2)) return;
	}

	m_collisionIt = m_collisions.begin();
	// Iternate through CollisionType combinations we want to recognize
	for (; m_collisionIt != m_collisions.end(); m_collisionIt++)
	{
		Collision c = *m_collisionIt;
		// If t1 and t2 match a recognized CollisionType combination
		if ((c.Type1 == t1 && c.Type2 == t2))
		{
			// Calculate the distance between the two objects
			Vector2 difference = pGameObject1->GetPosition() - pGameObject2->GetPosition();

			float radiiSum = pGameObject1->GetCollisionRadius() + pGameObject2->GetCollisionRadius();
			float radiiSumSquared = radiiSum * radiiSum; // Square the sum of the two radii
			
			// Check if the square of the difference is less than the square of the two radii
			// Saves CPU by not having to calculate the square root
			if (difference.LengthSquared() <= radiiSumSquared)
			{
				// Collision is detected, execute collision callback in the original order
				if (!swapped) c.Callback(pGameObject1, pGameObject2);
				else c.Callback(pGameObject2, pGameObject1);
			}
			return;
		}
	}
	// Unrecognized CollisionType combination, add it to the NonCollision vector
	//  to save CPU for future collisions like this.
	AddNonCollisionType(t1, t2);
}

void CollisionManager::AddNonCollisionType(const CollisionType type1, const CollisionType type2)
{
	NonCollision nc;
	nc.Type1 = (type1 < type2) ? type1 : type2;
	nc.Type2 = (type1 > type2) ? type1 : type2;
	m_nonCollisions.push_back(nc);
}