
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	// Wait for ship's delay to elapse.
	if (m_delaySeconds > 0)
	{
		// Reduce remaining delay by how much game time has elapsed since the last frame.
		m_delaySeconds -= pGameTime->GetTimeElapsed();
		// If the delay has elapsed, activate the ship.
		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();
		}
	}

	// Check if the ship is active.
	if (IsActive())
	{
		// Track the time the ship has been active
		m_activationSeconds += pGameTime->GetTimeElapsed();
		// If the ship has been active for more than 2 seconds and is off the screen, deactivate it.
		// Using activationSeconds ensures we aren't deactivating a ship that just became active off screen.
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	// Perform the base class Update method.
	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}