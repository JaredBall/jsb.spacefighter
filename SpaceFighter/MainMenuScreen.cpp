
#include <string>
#include "MainMenuScreen.h"
#include "GameplayScreen.h"

// Callback Functions
void OnStartGameSelect(MenuScreen *pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen());
}

void OnQuitSelect(MenuScreen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	pMainMenuScreen->SetQuitFlag();
	pMainMenuScreen->Exit();
}

void OnScreenRemove(Screen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	if (pMainMenuScreen->IsQuittingGame()) pScreen->GetGame()->Quit();
}



MainMenuScreen::MainMenuScreen()
{
	m_pTexture = nullptr;

	SetRemoveCallback(OnScreenRemove);

	SetTransitionInTime(2.0f);
	SetTransitionOutTime(0.5f);

	Show(); // Show the screen
}

void MainMenuScreen::LoadContent(ResourceManager *pResourceManager)
{
	// Logo
	m_pTexture = pResourceManager->Load<Texture>("Textures\\Logo.png");
	m_texturePosition = Game::GetScreenCenter() - Vector2::UNIT_Y * 150; // Offset texture 150 units higher

	// Create the menu items
	const int COUNT = 2;
	MenuItem *pItem;

	// Define font for menu item text.
	Font::SetLoadSize(20, true);
	Font *pFont = pResourceManager->Load<Font>("Fonts\\ariali.ttf"); // Default Ethnocentric.ttf

	// Set how many menu items there will be
	SetDisplayCount(COUNT);

	enum Items { START_GAME, QUIT }; // enum values match array position of menu items.
	std::string text[COUNT] = { "Start Game", "Quit" }; // Create array of menu item texts.

	for (int i = 0; i < COUNT; i++)
	{
		// Construct a menu item.
		pItem = new MenuItem(text[i]); // Set item text from array.
		pItem->SetPosition(Vector2(100, 100 + 50 * i)); // Set position on the screen based on loop index.
		pItem->SetFont(pFont); // Set item text font.
		pItem->SetColor(Color::Blue); // Set item text color.
		pItem->SetSelected(i == 0); // Set item as selected if the loop index is 0.
		AddMenuItem(pItem); // Add the menu item to the screen.
	}

	// Set functions to run when menu items are selected
	GetMenuItem(START_GAME)->SetSelectCallback(OnStartGameSelect);
	GetMenuItem(QUIT)->SetSelectCallback(OnQuitSelect);
}

void MainMenuScreen::Update(const GameTime *pGameTime)
{
	MenuItem *pItem;

	// Set the menu item colors
	for (int i = 0; i < GetDisplayCount(); i++)
	{
		pItem = GetMenuItem(i);
		pItem->SetAlpha(GetAlpha());

		if (pItem->IsSelected()) pItem->SetColor(SelectedItemColor);
		else pItem->SetColor(MenuItemColor);
	}

	MenuScreen::Update(pGameTime);
}

void MainMenuScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();
	pSpriteBatch->Draw(m_pTexture, m_texturePosition, Color::White * GetAlpha(), m_pTexture->GetCenter());
	pSpriteBatch->End();

	MenuScreen::Draw(pSpriteBatch);
}
